package com.company;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {
        // write your code here
        java.io.File file = new java.io.File("input.txt");

        hitung hitung = new hitung();
        Scanner input = new Scanner(System.in);

        try {
            java.io.PrintWriter output = new java.io.PrintWriter(file);
            System.out.println("Kode Satuan");
            System.out.println("1. Kilometer");
            System.out.println("2. Meter");
            System.out.println("3. Centimeter");
            System.out.print("pilih kode panjang yang akan dikonversi <1/2/3> : ");
            int pil = input.nextInt();
            System.out.print("pilih kode panjang hasil dikonversi <1/2/3> : ");
            int pill = input.nextInt();
            switch (pil){
                case 1:
                    if (pill == 1){
                        System.out.println("Konversi dari KiloMeter ke KiloMeter ?");
                    }
                    else if (pill == 2 ){
                        output.println("Konversi dari KiloMeter ke Meter");
                        System.out.print("Masukkan angka dalam KiloMeter : ");
                        float a = input.nextFloat();
                        output.println("Konversi dari "+ a + " KM menjadi  " + hitung.getHitungkmtom(a)+  " M");
                    }
                    else if (pill == 3 ){
                        output.println("Konversi dari KiloMeter ke CentiMeter");
                        System.out.print("Masukkan angka dalam KiloMeter : ");
                        float a = input.nextFloat();
                        output.println("Konversi dari "+ a + " KM menjadi " + hitung.getHitungkmtocm(a)+  " CM");
                    }
                    else {
                        System.out.println("Masukkan dengan benar");
                    }
                    break;
                case 2:
                    if (pill == 1){
                        output.println("Konversi dari Meter ke Kilometer");
                        System.out.print("Masukkan angka dalam Meter : ");
                        float a = input.nextFloat();
                        output.println("Konversi dari "+ a + " M Ke " + hitung.getHitungmtokm(a)+  " KM");
                    }
                    else if (pill == 2){
                        System.out.println("Konversi dari Meter ke Meter ?");
                    }
                    else if (pill == 3){
                        output.println("Konversi dari Meter ke Centi Meter");
                        System.out.print("Masukkan angka dalam Meter : ");
                        float a = input.nextFloat();
                        output.println("Konversi dari "+ a + " M Ke " + hitung.getHitungmtocm(a)+  " CM");
                    }
                    else {
                        System.out.println("Masukkan dengan benar");
                    }
                    break;
                case 3:
                    if (pill == 1){
                        output.println("Konversi dari CentiMeter ke KiloMeter");
                        System.out.print("Masukkan angka dalam CentiMeter : ");
                        float a = input.nextFloat();
                        output.println("Konversi dari "+ a + " CM Ke " + hitung.getHitungcmtokm(a)+  " KM");
                    }
                    else if (pill == 2){
                        output.println("Konversi dari CentiMeter ke Meter");
                        System.out.print("Masukkan angka dalam CentiMeter : ");
                        float a = input.nextFloat();
                        output.println("Konversi dari "+ a + " CM Ke " + hitung.getHitungcmtom(a)+  " M");
                    }
                    else if (pill == 3){
                        System.out.println("Konversi dari CentiMeter ke CentiMeter ?");
                    }
                    else  {
                        System.out.println("Masukkan dengan benar");
                    }
                    break;
            }
            output.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File myObj = new File("input.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()){
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        }catch (FileNotFoundException r){
            System.out.println("File Tidak Di Temukan");
            r.printStackTrace();
        }



    }
}